package com.walton.luke.zebratest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainAcitivity TAG";
    private long startTime;
    private int j;
    private TextView timeView;
    private long[] diffs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        timeView = findViewById(R.id.timeView);
        diffs = new long[21];
        Arrays.fill(diffs, 0);
        j = 0;

        Button btn = findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScanIntent("START_SCANNING");
            }
        });

        registerReceivers();
    }

    /**
     * Starts the loop to test scanner 20 times
     * @param extraValue
     */
    public void ScanIntent(String extraValue) {
        startTime = System.currentTimeMillis();
        Intent dwIntent = new Intent();
        dwIntent.setAction("com.symbol.datawedge.api.ACTION");
        dwIntent.putExtra("com.symbol.datawedge.api.SOFT_SCAN_TRIGGER", extraValue);
        dwIntent.putExtra("SEND_RESULT", "true");
        sendBroadcast(dwIntent);
    }

    /**
     * Registers the Intent receiver to get barcode data back from the hardware scanner
     */
    private void registerReceivers() {
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.walton.luke.scanning.ACTION");
        registerReceiver(myReceiver, filter);
    }

    /**
     * Broadcast receiver that is set to get only barcodes scanned
     */
    private BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            long endTime = System.currentTimeMillis();
            diffs[j] = endTime - startTime;
            j++;

            if (j < 21) {
                ScanIntent("START_SCANNING");
            } else {
                j = 0;
                printAll();
            }
        }
    };

    /**
     * Prints out the times taken to scan barcodes
     */
    private void printAll() {
        StringBuilder diffsString = new StringBuilder(60);
        for (int i = 0; i < 20; i++) {
            diffsString.append(diffs[i]).append("\n");
        }
        Log.e(TAG, diffsString.toString());
        timeView.setText(diffsString.toString());
    }

}
